<?php
namespace App\Repositories\Eloquent;

use App\Category;
use App\Contracts\Repositories\CategoryRepository as CategoryRepositoryInterface;

class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
	public function __construct(Category $category)
	{
		parent::__construct($category);
	}

	public function ofSlug($slug)
	{
		return $this->model()->ofSlug($slug)->firstOrFail();
	}
	
}