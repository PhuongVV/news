<?php
namespace App\Repositories\Eloquent;

use App\Comment;
use App\Contracts\Repositories\CommentRepository as CommentRepositoryInterface;

class CommentRepository extends AbstractRepository implements CommentRepositoryInterface
{
	public function __construct(Comment $comment)
	{
		parent::__construct($comment);
	}

	public function forArticle($id, $perPage = null)
	{
		return $this->model()->whereHas('article', function($query) use ($id) {
			$query->where('id', $id)->where('parent_id', '=', NULL);
		});
	}
}