<?php
namespace App\Repositories\Eloquent;

use App\Article;
use App\Contracts\Repositories\ArticleRepository as ArticleRepositoryInterface;

class ArticleRepository extends AbstractRepository implements ArticleRepositoryInterface
{
	public function __construct(Article $article)
	{
		parent::__construct($article);
	}

	public function ofSlug($slug)
	{
		return $this->model()->ofSlug($slug)->firstOrFail();
	}

}