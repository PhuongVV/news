<?php
namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
	/**
	 * @var Illuminate\Database\Eloquent\Model
	 */
	private $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function save(Model $model)
	{
		return $model->save();
	}

	public function create(array $attributes)
	{
		return $this->model()->create($attributes);
	}

	public function update(array $attributes)
	{
		return $this->model()->update($attributes);
	}

	public function find($id)
	{
		return $this->model()->findOrFail($id);
	}

	public function paginate($perPage = null)
	{
		return $this->model()->paginate($perPage);
	}

	protected function model()
	{
		return $this->model;
	}
}