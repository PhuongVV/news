<?php
namespace App\Repositories\Eloquent;

use App\Tag;
use App\Contracts\Repositories\TagRepository as TagRepositoryInterface;

class TagRepository extends AbstractRepository implements TagRepositoryInterface
{
	public function __construct(Tag $tag)
	{
		parent::__construct($tag);
	}
}