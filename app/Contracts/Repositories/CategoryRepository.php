<?php
namespace App\Contracts\Repositories;

interface CategoryRepository extends AbstractRepository
{
	public function ofSlug($slug);
}