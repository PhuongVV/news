<?php
namespace App\Contracts\Repositories;

interface ArticleRepository extends AbstractRepository
{
	public function ofSlug($slug);
}