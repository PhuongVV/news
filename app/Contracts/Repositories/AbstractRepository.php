<?php
namespace App\Contracts\Repositories;

interface AbstractRepository
{
	public function find($id);
}