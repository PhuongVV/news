<?php
namespace App;

use App\Traits\ScopeSlug;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Article extends Model
{
    use ScopeSlug;
    use Sluggable;

    protected $fillable = ['title', 'content', 'author'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    /**
     * Related articles having common tags
     * @return Collection of related articles
     */
    public function getRelatesAttribute()
    {
        return $this->where('id', '<>', $this->id)
                    ->whereHas('tags', function($query) {
                        $query->whereIn('id', $this->tags()->pluck('id'));
                    });
    }

    /**
     * Write new comment | Comment Factory
     * @param  array  $attributes comment's attributes
     * @return Comment             
     */
    public function newComment(array $attributes)
    {
        $comment = new Comment($attributes);
        $comment->article()->associate($this);
        return $comment;
    }

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

    public function tags()
    {
    	return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
    	return $this->hasMany(Comment::class);
    }
}
