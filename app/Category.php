<?php

namespace App;

use App\Traits\ScopeSlug;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use ScopeSlug;
    use Sluggable;

    protected $fillable = ['name'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }



    public function articles()
    {
    	return $this->hasMany(Article::class);
    }

    public function parent()
    {
    	return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
    	return $this->hasMany(Category::class, 'parent_id');
    }

    public function isRoot()
    {
        return $this->parent == null;
    }

    public function getRootAttribute()
    {
        return self::computeRoot($this);
    }

    private static function computeRoot(Category $category)
    {
        if( ! $category->isRoot()) {
            self::computeRoot($category->parent);
        }
        return $category->parent;
    }
}
