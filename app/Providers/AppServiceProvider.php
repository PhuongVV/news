<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindRepository();
    }

    /**
     * Method use to bind repositories to IoC
     * @return void
     */
    protected function bindRepository()
    {
        $repositories = [
            'App\Contracts\Repositories\ArticleRepository' => 'App\Repositories\Eloquent\ArticleRepository',
            'App\Contracts\Repositories\CategoryRepository' => 'App\Repositories\Eloquent\CategoryRepository',
            'App\Contracts\Repositories\CommentRepository' => 'App\Repositories\Eloquent\CommentRepository',
            'App\Contracts\Repositories\TagRepository' => 'App\Repositories\Eloquent\TagRepository'
        ];

        foreach($repositories as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
