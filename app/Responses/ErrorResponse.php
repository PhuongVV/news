<?php
namespace App\Responses;

class ErrorResponse
{
	private $code;
	private $message;

	public function __construct($code, $message)
	{
		$this->code = $code;
		$this->message = $message;
	}

	public function getResponse()
	{
		return [
			'error' => [
                'code' => $this->getCode(),
                'message' => $this->getMessage(),
            ]
		];
	}


    /**
     * Gets the value of code.
     *
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Gets the value of message.
     *
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }
}