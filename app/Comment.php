<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['content', 'author'];

    public function article()
    {
    	return $this->belongsTo(Article::class);
    }

    public function parent()
    {
    	return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function replies()
    {
    	return $this->hasMany(Comment::class, 'parent_id');
    }
}
