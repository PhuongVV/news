<?php
namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\CategoryRepository;
use App\Http\Requests;
use App\Responses\ErrorResponse;
use App\Traits\FractalResponse;
use App\Transformers\ApiCategoryTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CategoryController extends BaseController
{
	use FractalResponse;

	private $categories;

	public function __construct(CategoryRepository $categories)
	{
		$this->categories = $categories;
	}

    public function index()
    {
        try {
            $categories = $this->categories->paginate();
            $data =  $this->responsePagination($categories, new ApiCategoryTransformer($this->getApiPrefix()), 'categories');

            return response()->json($data, 200);
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }

    public function show($id)
    {
    	try {
    		$category = $this->categories->find($id);
	    	$data =  $this->responseItem($category, new ApiCategoryTransformer($this->getApiPrefix()), 'category', $with = ['parent', 'root', 'children']);

	    	return response()->json($data, 200);
    	} catch(ModelNotFoundException $e) {
    		$error = new ErrorResponse(404, $e->getMessage());
    		return response()->json($error->getResponse(), 404);
    	}
    }
}
