<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
	private $apiPrefix = 'v1';
	private $apiVersion = '1.0.0';

	

	public function getApiVersion()
	{
		return $this->apiVersion;
	}

    /**
     * Gets the value of apiPrefix.
     *
     * @return mixed
     */
    public function getApiPrefix()
    {
        return $this->apiPrefix;
    }
}