<?php
namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\TagRepository;
use App\Http\Requests;
use App\Responses\ErrorResponse;
use App\Traits\FractalResponse;
use App\Transformers\ApiTagTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TagController extends BaseController
{
    use FractalResponse;

    private $tags;
 	
 	public function __construct(TagRepository $tags)
    {
    	$this->tags = $tags;
    }	

    public function index()
    {
        try {
            $tags = $this->tags->paginate();
            $data =  $this->responsePagination($tags, new ApiTagTransformer($this->getApiPrefix()), 'tags');

            return response()->json($data, 200);
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }

    public function show($id)
    {
    	try {
    		$tag = $this->tags->find($id);
	    	$data =  $this->responseItem($tag, new ApiTagTransformer($this->getApiPrefix()), 'tag');

	    	return response()->json($data, 200);
    	} catch(ModelNotFoundException $e) {
    		$error = new ErrorResponse(404, $e->getMessage());
    		return response()->json($error->getResponse(), 404);
    	}
    }
}
