<?php
namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\ArticleRepository;
use App\Contracts\Repositories\CommentRepository;
use App\Http\Requests;
use App\Responses\ErrorResponse;
use App\Traits\FractalResponse;
use App\Transformers\ApiArticleTransformer;
use App\Transformers\ApiCommentTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{
	use FractalResponse;

	protected $articles;
    protected $comments;

    public function __construct(ArticleRepository $articles, CommentRepository $comments)
    {
    	$this->articles = $articles;
        $this->comments = $comments;
    }

    public function index()
    {
        try {
            $articles = $this->articles->paginate();
            $data =  $this->responsePagination($articles, new ApiArticleTransformer($this->getApiPrefix()), 'articles');

            return response()->json($data, 200);
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }

    public function show($id)
    {
    	try {
    		$article = $this->articles->find($id);
	    	$data =  $this->responseItem($article, new ApiArticleTransformer($this->getApiPrefix()), 'article', $with = ['category', 'tags', 'relates']);

	    	return response()->json($data, 200);
    	} catch(ModelNotFoundException $e) {
    		$error = new ErrorResponse(404, $e->getMessage());
    		return response()->json($error->getResponse(), 404);
    	}
    }

    public function writeComment(Request $request, $id)
    {
        try {
            $article = $this->articles->find($id);
            $comment = $article->newComment($request->all());
            $this->comments->save($comment);
            
            return redirect( route('api.v1.comment.show', ['id' => $comment->id]) );
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }

    public function comments($id)
    {
        try {
            $comments = $this->comments->forArticle($id)->paginate();
            $data =  $this->responsePagination($comments, new ApiCommentTransformer($this->getApiPrefix()), 'comment');

            return response()->json($data, 200);
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }

}
