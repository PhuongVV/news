<?php
namespace App\Http\Controllers\Api\V1;

use App\Contracts\Repositories\CommentRepository;
use App\Http\Requests;
use App\Responses\ErrorResponse;
use App\Traits\FractalResponse;
use App\Transformers\ApiCommentTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CommentController extends BaseController
{
    use FractalResponse;

    private $comments;
 	
 	public function __construct(CommentRepository $comments)
    {
    	$this->comments = $comments;
    }	

    public function show($id)
    {
        try {
            $comments = $this->comments->find($id);
            $data =  $this->responseItem($comments, new ApiCommentTransformer($this->getApiPrefix()), 'comment', $with = ['replies']);

            return response()->json($data, 200);
        } catch(ModelNotFoundException $e) {
            $error = new ErrorResponse(404, $e->getMessage());
            return response()->json($error->getResponse(), 404);
        }
    }
}
