<?php
namespace App\Transformers;

use App\Comment;
use App\Traits\FractalResponse;
use League\Fractal\TransformerAbstract;

class ApiCommentTransformer extends TransformerAbstract
{
	use FractalResponse;

	private $api;

	protected $availableIncludes = [
        'replies'
    ];

    public function __construct($api)
	{
		$this->api = $api;
	}

	public function transform(Comment $comment)
	{
		return[
			'id' => $comment->id,
			'author' => $comment->author,
			'content' => $comment->content,
			'links' => [
				'self' => env('APP_URL')."/api/{$this->api}/comment/{$comment->id}",
			]
		];
	}

	public function includeReplies(Comment $comment)
	{
		return $this->fractalPaginate($comment->replies()->paginate(), new ApiCommentTransformer($this->api), 'reply')->getResource();
	}

}