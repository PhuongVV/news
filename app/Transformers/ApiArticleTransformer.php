<?php
namespace App\Transformers;

use App\Article;
use App\Traits\FractalResponse;
use League\Fractal\TransformerAbstract;

class ApiArticleTransformer extends TransformerAbstract
{
	use FractalResponse;

	private $api;

	protected $availableIncludes = [
        'category', 'tags', 'relates'
    ];

    public function __construct($api)
	{
		$this->api = $api;
	}

	public function transform(Article $article)
	{
		return[
			'id' => $article->id,
			'slug' => $article->slug,
			'title' => $article->title,
			'content' => $article->content,
			'author' => $article->author,
			'links' => [
				'self' => env('APP_URL')."/api/{$this->api}/article/{$article->id}",
			]
		];
	}

	public function includeRelates(Article $article)
	{
		return $this->fractalPaginate($article->relates->paginate(), new ApiArticleTransformer($this->api), 'relate')->getResource();
	}

	public function includeCategory(Article $article)
    {
        return $this->item($article->category, new ApiCategoryTransformer($this->api), 'category');
    }

    public function includeTags(Article $article)
    {
        return $this->collection($article->tags, new ApiTagTransformer($this->api), 'tag');
    }
}