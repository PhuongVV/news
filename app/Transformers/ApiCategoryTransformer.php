<?php
namespace App\Transformers;

use App\Category;
use App\Traits\FractalResponse;
use League\Fractal\TransformerAbstract;

class ApiCategoryTransformer extends TransformerAbstract
{
	use FractalResponse;

	private $api;

	protected $availableIncludes = [
        'parent', 'root', 'children'
    ];

    public function __construct($api)
	{
		$this->api = $api;
	}

	public function transform(Category $category)
	{
		return[
			'id' => $category->id,
			'name' => $category->name,
			'links' => [
				'self' => env('APP_URL')."/api/{$this->api}/category/{$category->id}",
			]
		];
	}

	public function includeParent(Category $category)
	{
		if(!! $category->parent)
			return $this->item($category->parent, new ApiCategoryTransformer($this->api), 'parent');
	}

	public function includeRoot(Category $category)
	{
		if(!! $category->root)
			return $this->item($category->root, new ApiCategoryTransformer($this->api), 'root');
	}

	public function includeChildren(Category $category)
	{
			return $this->fractalPaginate($category->children()->paginate(), new ApiCategoryTransformer($this->api), 'child')
						->getResource();
	}

}