<?php
namespace App\Transformers;

use App\Tag;
use League\Fractal\TransformerAbstract;

class ApiTagTransformer extends TransformerAbstract
{
	private $api;

	public function __construct($api)
	{
		$this->api = $api;
	}

	public function transform(Tag $tag)
	{
		return[
			'id' => $tag->id,
			'name' => $tag->name,
			'links' => [
				'self' => env('APP_URL')."/api/{$this->api}/tag/{$tag->id}",
			]
		];
	}

}