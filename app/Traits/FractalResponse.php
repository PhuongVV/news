<?php
namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

use League\Fractal\Resource\Item as FractalItem;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection as FractalCollection;

trait FractalResponse
{
    public function responseCollection(Collection $collection, $transformer, String $resourceName, $with = [])
    {
        $resource = new FractalCollection($collection, $transformer, $resourceName);
        return $this->fractal()->parseIncludes($with)->createData($resource)->toArray();
    }
    public function responseItem(Model $item, $transformer, String $resourceName, $with = [])
    {
        $resource = new FractalItem($item, $transformer, $resourceName);
        return $this->fractal()->parseIncludes($with)->createData($resource)->toArray();
    }
    public function responsePagination(LengthAwarePaginator $paginator, $transformer, String $resourceName, $with = [])
    {
        return $this->fractalPaginate($paginator, $transformer, $resourceName, $with)->toArray();
    }

    private function fractalPaginate(LengthAwarePaginator $paginator, $transformer, String $resourceName, $with = [])
    {
        $resource = new FractalCollection($paginator->getCollection(), $transformer, $resourceName);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return $this->fractal()
                    ->parseIncludes($with)
                    ->createData($resource);
    }

    private function fractal()
    {
        $manager = new \League\Fractal\Manager();
        return $manager->setSerializer(new DataArraySerializer());
    }
}