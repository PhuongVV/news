<?php
namespace App\Traits;

trait ScopeSlug
{
	/**
     * Scope a query to find by slug
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }
}