<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {
	Route::get('/article', ['as' => 'api.v1.article.index', 'uses' => 'Api\V1\ArticleController@index']);
	Route::get('/article/{id}', ['as' => 'api.v1.article.show', 'uses' => 'Api\V1\ArticleController@show']);

	Route::get('/article/{id}/comment', ['as' => 'api.v1.article.comments', 'uses' => 'Api\V1\ArticleController@comments']);
	Route::post('/article/{id}/comment', ['as' => 'api.v1.article.write-comment', 'uses' => 'Api\V1\ArticleController@writeComment']);

	Route::get('/tag', ['as' => 'api.v1.tag.index', 'uses' => 'Api\V1\TagController@index']);
	Route::get('/tag/{id}', ['as' => 'api.v1.tag.show', 'uses' => 'Api\V1\TagController@show']);


	Route::get('/comment/{id}', ['as' => 'api.v1.comment.show', 'uses' => 'Api\V1\CommentController@show']);

	Route::get('/category', ['as' => 'api.v1.category.index', 'uses' => 'Api\V1\CategoryController@index']);
	Route::get('/category/{id}', ['as' => 'api.v1.category.show', 'uses' => 'Api\V1\CategoryController@show']);
});


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
