<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class, 100)->create()->each(function($category) {
        	$category->children()->saveMany(factory(App\Category::class, 30)->make());
        });
    }
}
