<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Comment::class, 100)->make()->each(function($comment) {
        	$article = App\Article::inRandomOrder()->first();
        	$comment->article()->associate($article);
        	$comment->save();
        	
        	$comment->replies()->saveMany(factory(App\Comment::class, 50)->make()->each(function($reply) use($article) {
        		$reply->article()->associate($article);
        		$reply->save();
        	}));
        });
    }
}
