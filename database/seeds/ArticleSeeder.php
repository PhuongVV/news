<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Article::class, 100)->create()->each(function($article) {
        	$article->category()->associate( App\Category::inRandomOrder()->first() );
            $article->tags()->saveMany( App\Tag::inRandomOrder()->limit(rand(0, 30))->get() );
        	$article->save();
        });
    }


     


}
