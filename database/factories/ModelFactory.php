<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word(),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->words(3, true),
    ];
});


$factory->define(App\Article::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->words(10, true),
        'content' => $faker->paragraph(10),
        'author' => $faker->name(),
    ];
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->paragraph(10),
        'author' => $faker->name(),
    ];
});
